#!/usr/bin/env bash

set -e

if [ "$1" == "/usr/bin/supervisord" ]; then
    if [ ! -e index.php ]; then
        echo >&2 "Php app not found in $PWD - copying now..."
        cp -r /usr/src/app/* /var/www/html/
        echo >&2 "... Php app has been successfully copied to $PWD"

        echo >&2 "Fixing files ownership..."
        chmod -R 755 /var/www/html
        chown -R www-data:www-data /var/www/html
        echo >&2 "...done"

        echo >&2 "Complete! Php app is ready to use!"
    fi
fi

exec "$@"
